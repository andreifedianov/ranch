package com.andreifedianov.android.ranch.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.andreifedianov.android.ranch.http.HttpBin;
import com.andreifedianov.android.ranch.models.Constants;
import com.andreifedianov.android.ranch.models.FoursquareVenue;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class FoursquareService extends IntentService {
    public static final String TRANSACTION_DONE = "com.andreifedianov.android.celery.services.action.FOURSQUARE_DONE";
    public static final String FOURSQUARE_IMAGE = "com.andreifedianov.android.celery.services.action.FOURSQUARE_IMAGE_DONE";
    public static final String EXTRA_FOURSQUARE_IMAGE_URL = "com.andreifedianov.android.celery.services.extra.FOURSQUARE_IMAGE_URL";
    private static final String TAG = "FoursquareService";
    private static final String EXTRA_FOURSQUARE_ID = "com.andreifedianov.android.celery.services.extra.FID_PARAM";
    private static final String EXTRA_LAT = "com.andreifedianov.android.celery.services.extra.LAT";
    private static final String EXTRA_LON = "com.andreifedianov.android.celery.services.extra.LON";

    private static final String ACTION_GET_VENUES = "com.andreifedianov.android.celery.services.action.GET_VENUES";
    private static final String ACTION_GET_IMAGE = "com.andreifedianov.android.celery.services.action.GET_VENUE_IMAGE";


    private ArrayList<FoursquareVenue> mVenueList = new ArrayList<>();
    private ArrayList<String> mTitleList = new ArrayList<>();


    public FoursquareService() {
        super("FoursquareService");
    }

    public static void startActionGetVenues(Context context, Double lat, Double lon) {
        Intent intent = new Intent(context, FoursquareService.class);
        intent.setAction(ACTION_GET_VENUES);
        intent.putExtra(EXTRA_LAT, lat);
        intent.putExtra(EXTRA_LON, lon);
        context.startService(intent);
    }

    public static void startActionGetImage(Context context, String param1, int height) {
        Intent intent = new Intent(context, FoursquareService.class);
        intent.setAction(ACTION_GET_IMAGE);
        intent.putExtra(EXTRA_FOURSQUARE_ID, param1);
        intent.putExtra("height", "" + height);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_VENUES.equals(action)) {
                final Double lat = intent.getDoubleExtra(EXTRA_LAT, 0);
                final Double lon = intent.getDoubleExtra(EXTRA_LON, 0);
                handleActionGetVenues(lat, lon);
            } else if (ACTION_GET_IMAGE.equals(action)) {
                final String foursquareId = intent.getStringExtra(EXTRA_FOURSQUARE_ID);
                final String height = intent.getStringExtra("height");
                handleActionGetBackgroundImage(foursquareId, height);
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionGetVenues(Double lat, Double lon) {
        Log.d("FoursquareService", "Service Started");
        Log.d("FoursquareService", lat + " " + lon);

        mVenueList = parseFoursquare(doSearch("" + lat + "," + lon));

        Intent done = new Intent(TRANSACTION_DONE);

        StringBuilder b;
        for (FoursquareVenue v : mVenueList) {
            b = new StringBuilder();

            b.append(v.getName());

//            if (!empty(v.getCity())) {
//                b.append(", ");
//                b.append(v.getCity());
//            }
            mTitleList.add(b.toString());
        }

        done.putParcelableArrayListExtra("venueList", mVenueList);
        done.putStringArrayListExtra("titleList", mTitleList);

        FoursquareService.this.sendBroadcast(done);
        Log.d("FoursquareService", "Service Finished");
    }

    private void handleActionGetBackgroundImage(String foursquareId, String height) {
        //https://api.foursquare.com/v2/venues/
        // 4b05e882f964a52096e522e3/
        // photos
        // ?client_id=KFFRYVMLJMY4B0H3PDSUUX0QIZBPNLXDKXHQMNZ2F2NHLHBP
        // &client_secret=O4Q4UUP1TMASWJEKCB4XF5V5EWYKQCR4WW4ICUM4AFDYV5P2
        // &v=20150120
        // &venue=checkin
        // &limit=1
        String result;
        StringBuilder url = new StringBuilder();
        url.append(Constants.FOURSQUARE_IMAGE_URL)
                .append(foursquareId)
                .append(Constants.PHOTOS)
                .append("?client_id=").append(Constants.CLIENT_ID)
                .append("&client_secret=").append(Constants.CLIENT_SECRET)
                .append("&v=").append(Constants.version)
                .append("&venue=checkin")
                .append("&limit=1");
        result = makePhotoUrl(HttpBin.getInstance().get(url.toString()), height);
        Intent done = new Intent(FOURSQUARE_IMAGE);
        done.putExtra(EXTRA_FOURSQUARE_IMAGE_URL, result);
        FoursquareService.this.sendBroadcast(done);
    }

    private String makePhotoUrl(String input, String height) {
        StringBuilder result = new StringBuilder();
        try {
            JSONObject obj = new JSONObject(input);
            if (obj.has("meta") && obj.getJSONObject("meta").getString("code").equalsIgnoreCase("200")) { //returns a 200 = OK
                JSONArray jarray = obj.getJSONObject("response").getJSONObject("photos").getJSONArray("items");
                obj = jarray.getJSONObject(0);
                result.append(obj.getString("prefix"))
                        .append("height")
                        .append(height)
                        .append(obj.getString("suffix"));
            }

            return result.toString();
        } catch (Exception e) {

        }
        return null;
    }

    public String doSearch(String location) {
        String result;
        StringBuilder url = new StringBuilder();
        //https://api.foursquare.com/v2/venues/search/33.89340957 -84.28405526
        url.append(Constants.FOURSQUARE_URL)
                .append("?client_id=").append(Constants.CLIENT_ID)
                .append("&client_secret=").append(Constants.CLIENT_SECRET)
                .append("&v=").append(Constants.version)
                .append("&ll=").append(location)
//                .append("&&categoryId=").append(Constants.category_food)
                .append("&venue=checkin");
        result = HttpBin.getInstance().get(url.toString());
        return result;
    }

    private ArrayList<FoursquareVenue> parseFoursquare(final String response) {

        ArrayList<FoursquareVenue> temp = new ArrayList<>();
        try {
            // make an jsonObject in order to parse the response
            JSONObject jsonObject = new JSONObject(response);

            // make an jsonObject in order to parse the response
            if (jsonObject.has("response")) {
                if (jsonObject.getJSONObject("response").has("venues")) {
                    JSONArray jsonArray = jsonObject.getJSONObject("response").getJSONArray("venues");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        FoursquareVenue poi = new FoursquareVenue();
                        if (jsonArray.getJSONObject(i).has("name") && jsonArray.getJSONObject(i).has("id")) {
                            poi.setId(jsonArray.getJSONObject(i).getString("id"));
                            poi.setName(jsonArray.getJSONObject(i).getString("name"));
                            if (jsonArray.getJSONObject(i).has("location")) {

                                if (jsonArray.getJSONObject(i).getJSONObject("location").has("address")) {
                                    poi.setAddress(jsonArray.getJSONObject(i).getJSONObject("location").getString("address"));
                                }

                                if (jsonArray.getJSONObject(i).getJSONObject("location").has("distance")) {
                                    poi.setDist(jsonArray.getJSONObject(i).getJSONObject("location").getString("distance"));
                                }

                                if (jsonArray.getJSONObject(i).getJSONObject("location").has("city")) {
                                    poi.setCity(jsonArray.getJSONObject(i).getJSONObject("location").getString("city"));
                                }
                                if (jsonArray.getJSONObject(i).has("categories")) {
                                    if (jsonArray.getJSONObject(i).getJSONArray("categories").length() > 0) {
                                        if (jsonArray.getJSONObject(i).getJSONArray("categories").getJSONObject(0).has("icon")) {
                                            poi.setCategory(jsonArray.getJSONObject(i).getJSONArray("categories").getJSONObject(0).getString("name"));
                                        }
                                    }
                                }
                                temp.add(poi);
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
        return temp;

    }
}