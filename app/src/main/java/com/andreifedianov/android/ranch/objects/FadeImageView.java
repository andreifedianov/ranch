package com.andreifedianov.android.ranch.objects;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;


public class FadeImageView extends ImageView {

    private static final int FADE_IN_TIME_MS = 500;

    public FadeImageView(Context context) {
        super(context);
    }

    public FadeImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FadeImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public FadeImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        Drawable[] draw;
        TransitionDrawable td;
        if (getDrawable() == null) {
            draw = new Drawable[]{
                    new ColorDrawable(android.R.color.transparent),
                    new BitmapDrawable(getContext().getResources(), bm)
            };
        } else {
            draw = new Drawable[]{
                    getDrawable(),
                    new BitmapDrawable(getContext().getResources(), bm)
            };
        }
        td = new TransitionDrawable(draw);
        td.setCrossFadeEnabled(true);
        setImageDrawable(td);
        td.startTransition(FADE_IN_TIME_MS);
    }
}
