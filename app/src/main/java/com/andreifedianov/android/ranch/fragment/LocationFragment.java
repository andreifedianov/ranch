package com.andreifedianov.android.ranch.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;

import com.andreifedianov.android.ranch.R;
import com.andreifedianov.android.ranch.main.MainActivity;
import com.andreifedianov.android.ranch.models.FoursquareVenue;
import com.andreifedianov.android.ranch.models.Tip;
import com.andreifedianov.android.ranch.objects.CustomTextView;
import com.andreifedianov.android.ranch.services.FoursquareService;
import com.andreifedianov.android.ranch.services.LocationService;

import java.util.ArrayList;
import java.util.Date;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the interface.
 */
public class LocationFragment extends Fragment implements View.OnClickListener, AbsListView.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener, AbsListView.OnScrollListener {

    private static final String TAG = "LocationFragment";


    private OnLocationFragmentInteractionListener mListener;
    /**
     * ****
     * Data
     * ****
     */
    private ArrayList<FoursquareVenue> mVenueList = null;
    private ArrayList<String> mTitleList = null;

    private int selectedItem = -1;
    /**
     * *********
     * Viewables
     * *********
     */
    private AbsListView mListView;
    private SwipeRefreshLayout swipeView;
    private ListAdapter mAdapter;
    private BroadcastReceiver foursquareVenueListReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Log.d("Foursquare", "Broadcast Received");

                mVenueList = intent.getParcelableArrayListExtra("venueList");
                mTitleList = intent.getStringArrayListExtra("titleList");
                if (mVenueList != null && mTitleList != null) {
                    ((MainActivity) getActivity()).setmVenueList(mVenueList);
                    ((MainActivity) getActivity()).setmTitleList(mTitleList);
                }
                if (mListView != null) {
                    mStatusLayout.setVisibility(LinearLayout.GONE);
                    mAdapter = new ArrayAdapter<>(getActivity(), R.layout.fragment_location_entry, R.id.location_entry_text, mTitleList);
                    mListView.setAdapter(mAdapter);
                    mListView.animate().setDuration(500).alpha(100).start();
                    mListView.clearAnimation();
                    swipeView.setRefreshing(false);
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }


        }
    };
    private ImageButton mSkipToHistory;
    private Button mContinueButton;
    private CustomTextView mSelectedLocation;
    private ProgressBar mProgressBar;
    private LinearLayout mStatusLayout;
    /**
     * *******
     * Objects
     * *******
     */
    private Context mCtx;
    private BroadcastReceiver locationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(LocationService.TRANSACTION_DONE)) {
                double lat = intent.getDoubleExtra("lat", 0.0);
                double lon = intent.getDoubleExtra("lon", 0.0);
                Log.d("LOCATION", "mLocation=" + lat + "," + lon);
                FoursquareService.startActionGetVenues(mCtx, lat, lon);
            } else if (intent.getAction().equalsIgnoreCase(LocationService.TRANSACTION_LOCATION_SERVICES_OFF)) {
                //Add a Location Not Found Fragment?
            }
        }
    };
    private DisplayMetrics dm = new DisplayMetrics();

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public LocationFragment() {
    }

    public static LocationFragment newInstance() {
        LocationFragment fragment = new LocationFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        mCtx = getActivity().getApplicationContext();
        mTitleList = ((MainActivity) getActivity()).getmTitleList();
        mVenueList = ((MainActivity) getActivity()).getmVenueList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.fragment_location_list, container, false);
        mStatusLayout = (LinearLayout) view.findViewById(R.id.statusLayout);
        mSkipToHistory = (ImageButton) view.findViewById(R.id.skipToHistory);
        mSkipToHistory.setOnClickListener(this);
        mSelectedLocation = (CustomTextView) view.findViewById(R.id.selectedLocation);
        mContinueButton = (Button) view.findViewById(R.id.btnContinue);
        mContinueButton.setOnClickListener(this);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        swipeView = (SwipeRefreshLayout) view.findViewById(R.id.location_list_swipe);
        swipeView.setEnabled(false);
        swipeView.setOnRefreshListener(this);
        mListView = (AbsListView) view.findViewById(R.id.location_list);
        mListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);


        if (mTitleList != null && mVenueList != null && !mTitleList.isEmpty() && !mVenueList.isEmpty()) {
            Log.i(TAG, "not null");
            mStatusLayout.setVisibility(View.GONE);
            mAdapter = new ArrayAdapter<>(getActivity(), R.layout.fragment_location_entry, R.id.location_entry_text, mTitleList);
            mListView.setAdapter(mAdapter);
            if (selectedItem > -1) {
                mSelectedLocation.setVisibility(CustomTextView.VISIBLE);
                mSelectedLocation.setText(mVenueList.get(selectedItem).getName());
                mContinueButton.setText(getString(R.string.button_continue));
            }
        } else {
            Log.i(TAG, "startLocationService");
            mStatusLayout.setVisibility(View.VISIBLE);
            swipeView.setRefreshing(true);
            mCtx.startService(new Intent(mCtx, LocationService.class));
        }

        mListView.setOnItemClickListener(this);
        mListView.setOnScrollListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(TAG, "onAttach");

        try {
            mListener = (OnLocationFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart");
        IntentFilter locationFilter = new IntentFilter();
        locationFilter.addAction(LocationService.TRANSACTION_DONE);
        locationFilter.addAction(LocationService.TRANSACTION_LOCATION_SERVICES_OFF);
        mCtx.registerReceiver(locationReceiver, locationFilter);

        IntentFilter foursquareFilter = new IntentFilter();
        foursquareFilter.addAction(FoursquareService.TRANSACTION_DONE);
        mCtx.registerReceiver(foursquareVenueListReceiver, foursquareFilter);

        getActivity().setTitle(getResources().getString(R.string.title_fragment_location));
        super.onStart();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
        mCtx.unregisterReceiver(locationReceiver);
        mCtx.unregisterReceiver(foursquareVenueListReceiver);
        super.onStop();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("onActivityCreated", "onActivityCreated");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("onSaveInstanceState", "onSaveInstanceState");
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            Log.d("onItemClick", "Pos=" + position + " " + parent.getSelectedItemPosition() + ", id=" + id + " " + parent.getSelectedItemId());
            if (selectedItem == position) {
                return;
            }
            mListView.invalidate();
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
//            mSelectedPlace.setText(mVenueList.get(position).getName());

            ArrayList<FoursquareVenue> mVenueList = ((MainActivity) getActivity()).getmVenueList();

            Tip tip = ((MainActivity) getActivity()).getTip();
            tip.setfId(mVenueList.get(position).getId());
            tip.setDate(new Date());
            tip.setfId(mVenueList.get(position).getId());
            tip.setName(mVenueList.get(position).getName());
            tip.setLocation(mVenueList.get(position).getAddress() + ", " + mVenueList.get(position).getCity());
            ((MainActivity) getActivity()).setTip(tip);

            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
            int height = dm.heightPixels / 4;
            FoursquareService.startActionGetImage(mCtx, tip.getfId(), height);
            mContinueButton.setText(getString(R.string.button_continue));
            selectedItem = position;
            if (mSelectedLocation.getVisibility() == CustomTextView.GONE) {
                mSelectedLocation.setVisibility(CustomTextView.VISIBLE);
                Animation anim = AnimationUtils.loadAnimation(mCtx, R.anim.slide_down);
                mSelectedLocation.startAnimation(anim);
            }
            mSelectedLocation.setText(mVenueList.get(position).getName());
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("MenuItem", "" + item.getItemId());
        switch (item.getItemId()) {
        }
        return false;//super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.skipToHistory:
                mSkipToHistory.animate().setDuration(10).alpha(.5f).start();
                mListener.onLocationFragmentInteraction(new Uri.Builder().appendPath(this.getClass().toString()).appendQueryParameter("key", "skip").build());
                break;
            case R.id.btnContinue:
                mListener.onLocationFragmentInteraction(new Uri.Builder().appendPath(this.getClass().toString()).appendQueryParameter("key", "" + selectedItem).build());
                break;

        }
    }

    @Override
    public void onRefresh() {
        Log.d(TAG, "Refresh Requested");
        mListView.setAlpha(0);
        swipeView.setRefreshing(true);
        mCtx.startService(new Intent(mCtx, LocationService.class));
//        mStatusLayout.setVisibility(LinearLayout.VISIBLE);
        if (mSelectedLocation.getVisibility() == CustomTextView.VISIBLE) {
            Animation slideUp = AnimationUtils.loadAnimation(mCtx, R.anim.slide_up);
            mSelectedLocation.startAnimation(slideUp);
            mSelectedLocation.setVisibility(CustomTextView.GONE);
        }

        selectedItem = -1;
        mContinueButton.setText(getResources().getString(R.string.button_continue_skip));

//        mListView.setAnimation(Animation.);
//                (new Handler()).postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        swipeView.setRefreshing(false);
//
//                    }
//                }, 3000);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (firstVisibleItem == 0)
            swipeView.setEnabled(true);
        else
            swipeView.setEnabled(false);
    }

    public interface OnLocationFragmentInteractionListener {
        public void onLocationFragmentInteraction(Uri uri);
    }
}
