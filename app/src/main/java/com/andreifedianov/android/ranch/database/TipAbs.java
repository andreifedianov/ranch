package com.andreifedianov.android.ranch.database;

import android.provider.BaseColumns;

public abstract class TipAbs implements BaseColumns {
    public static final String TABLE_NAME = "entry";
    public static final String TIP_FID = "foursquareid";
    public static final String TIP_DATE = "date";
    public static final String TIP_TOTAL = "total";
    public static final String TIP_TIP = "tip";
    public static final String TIP_PERCENT = "percent";
    public static final String TIP_COORDS = "coords";
    public static final String TIP_LOCATION = "location";
    public static final String TIP_NAME = "name";
}