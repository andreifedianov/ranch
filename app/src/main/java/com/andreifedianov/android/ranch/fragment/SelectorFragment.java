package com.andreifedianov.android.ranch.fragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.andreifedianov.android.ranch.R;
import com.andreifedianov.android.ranch.database.TipOpenHelper;
import com.andreifedianov.android.ranch.main.MainActivity;
import com.andreifedianov.android.ranch.models.Constants;
import com.andreifedianov.android.ranch.models.Tip;
import com.andreifedianov.android.ranch.objects.CustomTextView;
import com.andreifedianov.android.ranch.view.TipSlider;

import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;

import static com.andreifedianov.android.util.Util.empty;
import static com.andreifedianov.android.util.Util.isNumeric;

public class SelectorFragment extends Fragment implements CustomDialogFragment.OnNumpadDialogListener, View.OnTouchListener, View.OnClickListener, TipSlider.OnSliderUpdate {

    private static final String TAG = "SelectorFragment";
    private static final double TAP_INTERVAL = 0.005; //0.5%

    private static final String ARG_VENUE = "VenueID";


    CustomTextView mInput = null;
    private double percent = 0.15;
    private double bill;
    private double total;
    private GestureDetectorCompat mGestureDetector = null;
    private GestureDetectorCompat mOutputGestureDetector = null;
    //Number Formatting
    private Locale mLocale = Locale.US;
    private NumberFormat mNumberFormat = NumberFormat.getInstance(mLocale);
    private NumberFormat mPercentageFormat = NumberFormat.getPercentInstance(mLocale);
    private NumberFormat mCurrencyFormat = NumberFormat.getCurrencyInstance(mLocale);
    private LinearLayout mTipLayout = null;
    private CustomTextView mTipPercent = null;
    private CustomTextView mTipAmount = null;
    private RelativeLayout mTipContainer = null;
    private CustomTextView mTotal = null;
    private ImageButton mNextButton = null;

    private TipSlider tipSlider;
    private Tip mTip = null;
    private TipOpenHelper mDbHelper;
    private OnTrackingFragmentInteractionListener mListener;
    private String current = "";

    private FragmentManager fm;

    public SelectorFragment() {
    }

    public static SelectorFragment newInstance(String pos) {
        SelectorFragment fragment = new SelectorFragment();
        Bundle args = new Bundle();
        args.putString(ARG_VENUE, pos);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
//        fm = getActivity().getSupportFragmentManager();
        fm = getChildFragmentManager();
//        if (savedInstanceState == null) {
//            fm.beginTransaction()
//                    .add(R.id.fragment_holder, SliderFragment.newInstance())
//                    .commit();
//        }
        mTip = ((MainActivity) getActivity()).getTip();


        mNumberFormat.setRoundingMode(RoundingMode.HALF_EVEN);
        mCurrencyFormat.setRoundingMode(RoundingMode.HALF_EVEN);
        mNumberFormat.setMaximumFractionDigits(2);
        mPercentageFormat.setMaximumFractionDigits(2);
        mPercentageFormat.setMinimumFractionDigits(1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View v = inflater.inflate(R.layout.fragment_tracking, container, false);

        tipSlider = (TipSlider) v.findViewById(R.id.tip_slider);
        mInput = (CustomTextView) v.findViewById(R.id.inputBill);
        mInput.setOnClickListener(this);
        mNextButton = (ImageButton) v.findViewById(R.id.nextButton);
        mTipContainer = (RelativeLayout) v.findViewById(R.id.tipContainer);
        mTotal = (CustomTextView) v.findViewById(R.id.outputTotal);
        mNextButton.setEnabled(false);
        mTipContainer.setOnTouchListener(this);
        mNextButton.setOnClickListener(this);
        mTotal.setOnTouchListener(this);
        tipSlider.setOnSliderUpdate(this);
//        mTipAmount.setText("$0.00");
//        mTipPercent.setText("15.0%");
        String title;
        if (getArguments() != null && Integer.parseInt(getArguments().getString(ARG_VENUE)) > -1) {
            title = ((MainActivity) getActivity()).getmVenueList().get(Integer.parseInt(getArguments().getString(ARG_VENUE))).getName();
        } else {
            title = getString(R.string.location_unavailable);
        }
        getActivity().setTitle(title);
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        Log.d(TAG, "onAttach");
        super.onAttach(activity);
        mDbHelper = new TipOpenHelper(activity.getApplicationContext());
//        mGestureDetector = new GestureDetectorCompat(activity.getApplicationContext(), new PercentSelectorGestureListener());
        mOutputGestureDetector = new GestureDetectorCompat(activity.getApplicationContext(), new OutputTotalGestureListener());
        try {
            mDbHelper.open();
            mListener = (OnTrackingFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick");
        switch (v.getId()) {
            case R.id.nextButton:
                saveTip();
                mDbHelper.createTip(mTip);
                mNextButton.animate().setDuration(10).alpha(.5f);
                mListener.onTrackingFragmentInteraction(new Uri.Builder().appendPath(this.getClass().toString()).appendQueryParameter("key", "" + getArguments().getString(ARG_VENUE)).build());
                break;
            case R.id.inputBill:
                CustomDialogFragment numpadFragment = new CustomDialogFragment();
                if (!empty(mInput.getText().toString())) {
                    Bundle args = new Bundle();
                    args.putString(Constants.NumpadDialogFragment.NUMPAD_DATA, mInput.getText().toString());
                    numpadFragment.setArguments(args);
                }
                numpadFragment.setTargetFragment(this, 0);
                numpadFragment.show(fm, "NumpadDialog");
                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.outputTotal:
                mOutputGestureDetector.onTouchEvent(event);
                return true;
        }
        return false;
    }

    private void saveTip() {
        percent = tipSlider.getPercent() / 100;
        mTip.setDate(new Date());
        mTip.setPercent(Double.valueOf(new DecimalFormat("#.####").format(percent)));
        mTip.setTip(Double.valueOf(new DecimalFormat("#.##").format(bill * percent))); //<<<<<<<<<<<<<< DECIMAL FORMAT
        mTip.setTotal(Double.valueOf(new DecimalFormat("#.##").format(bill)));

        ((MainActivity) getActivity()).setTip(mTip);
    }

    @Override
    public void onPositiveDialogClick(CustomDialogFragment dialog) {
        setNewInput(dialog.getAmount());
    }

    private void setNewInput(String cleanString) {
        if (isNumeric(cleanString)) {
            double parsed = Double.parseDouble(cleanString);
            bill = (parsed / 100);
            String formatted = NumberFormat.getCurrencyInstance().format((parsed / 100));
            mInput.setText(formatted);
            tipSlider.setAmount(bill);
            if (bill > 0 && !mNextButton.isEnabled()) {
                mNextButton.setEnabled(true);
            } else if (!mNextButton.isEnabled()) {
                mNextButton.setEnabled(false);
            }
        }
    }

    @Override
    public void setTotal(double total) {
        Log.d(TAG, "Total=" + total);
        this.total = total;
        mTotal.setText(NumberFormat.getCurrencyInstance().format(total));
    }

    public interface OnTrackingFragmentInteractionListener {
        public void onTrackingFragmentInteraction(Uri uri);
    }


    private class OutputTotalGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (bill <= 0) {
                return false;
            }
            tipSlider.updateTip(TipSlider.TOUCH_ROUND);
            return super.onSingleTapConfirmed(e);
        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent e) {
            Log.d(TAG, "DoubleTap Output Total to Enter Desired Total");
            return super.onDoubleTapEvent(e);
        }

        @Override
        public void onLongPress(MotionEvent e) {
            Log.d(TAG, "LongClick Output Total to Enter Desired Total");
            super.onLongPress(e);
        }
    }

//    private class PercentSelectorGestureListener extends GestureDetector.SimpleOnGestureListener {
//
//        private static final int SWIPE_MIN_DISTANCE = 120;
//        private static final int SWIPE_MAX_OFF_PATH = 250;
//        private static final int SWIPE_THRESHOLD_VELOCITY = 200;
//
//        public PercentSelectorGestureListener() {
//        }
//
//        @Override
//        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
//            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
//                //From Right to Left
//                return true;
//            } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
//                //From Left to Right
//                return true;
//            }
//            if (e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
//                //From Bottom to Top
//
//                return true;
//            } else if (e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE && Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
//                //From Top to Bottom
//                return true;
//            }
//            return false;
//        }
//
//        @Override
//        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
//            Log.d(TAG, "e1=" + e1.getY() + " " + e1.getAction() + ", e2=" + e2.getY() + " " + e2.getAction() + " ");
//
//            if (e1.getY() > (mTipContainer.getHeight() + (float) mTipLayout.getHeight() / 2) || e1.getY() < 0 || Math.abs(e2.getY() - mTipLayout.getY()) > SWIPE_MAX_OFF_PATH) {
//                return false;
//            }
//
//            if (e2.getY() > (mTipContainer.getHeight() - (float) mTipLayout.getHeight() / 2) || e2.getY() <= (mTipLayout.getHeight() / 2)) {
//                return false;
//            }
//            percent = Math.round(e2.getY() * (UPPER_LIMIT) / mTipContainer.getHeight()) / 100.0f;
//            setY(e2);
//            //h1=1431,h2=1695
//            return updateTip();
//        }
//    }

}
