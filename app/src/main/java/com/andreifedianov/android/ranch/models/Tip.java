package com.andreifedianov.android.ranch.models;

import java.util.Date;

public final class Tip {

    private long id;         //Sqlite3 _id
    private String fId;        //Unique Foursquare location ID, for statistical purposes
    private String name;       //Name of the physical location, i.e place
    private String location;   //Address, City
    private Date date;       //
    private double total;      //Bill Total (without Tip)
    private double tip;        //Tip itself
    private double percent;    //Tip Percent
    private String geoCoords;  //Probably not required

    public Tip() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getfId() {
        return fId;
    }

    public void setfId(String fId) {
        this.fId = fId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getGeoCoords() {
        return geoCoords;
    }

    public void setGeoCoords(String geoCoords) {
        this.geoCoords = geoCoords;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getTip() {
        return tip;
    }

    public void setTip(double tip) {
        this.tip = tip;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    @Override
    public String toString() {
        return "Tip{" +
                "id=" + id +
                ", fId='" + fId + '\'' +
                ", name='" + name + '\'' +
                ", location='" + location + '\'' +
                ", date=" + date +
                ", total=" + total +
                ", tip=" + tip +
                ", percent=" + percent +
                ", geoCoords='" + geoCoords + '\'' +
                '}';
    }
}
