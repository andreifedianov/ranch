package com.andreifedianov.android.ranch.objects;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.TextView;

import com.andreifedianov.android.ranch.R;

public class DurationTextView extends TextView {

    //    private static final String TEMPLATE = "Duration: <strong>%s</strong>";
    private String TEMPLATE;

    public DurationTextView(Context context) {
        super(context);
    }

    public DurationTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTemplate(context, attrs);
    }

    public DurationTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTemplate(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public DurationTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setTemplate(context, attrs);
    }


    private void setTemplate(Context context, AttributeSet attrs) {
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.TemplateTextView);
        TEMPLATE = attributes.getString(R.styleable.TemplateTextView_template);
        if (TEMPLATE == null || !TEMPLATE.contains("%s")) {
            TEMPLATE = "%s";
        }
        attributes.recycle();
    }

    /**
     * Updates the text of the view with the new duration, properly
     * formatted
     *
     * @param duration The duration in seconds
     */
    public void setDuration(float duration) {
        int durationInMinutes = Math.round(duration / 60);
        int hours = durationInMinutes / 60;
        int minutes = durationInMinutes % 60;

        String hourText = "";
        String minuteText = "";

        if (hours > 0) {
            hourText = hours + (hours == 1 ? " hour " : " hours ");
        }
        if (minutes > 0) {
            minuteText = minutes + (minutes == 1 ? " minute" : " minutes");
        }
        if (hours == 0 && minutes == 0) {
            minuteText = "less than 1 minute";
        }

        String durationText = String.format(TEMPLATE, hourText + minuteText);
        setText(Html.fromHtml(durationText), BufferType.SPANNABLE);
    }
}
