package com.andreifedianov.android.ranch.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.andreifedianov.android.ranch.models.Tip;

import java.sql.SQLException;

import static com.andreifedianov.android.util.Util.empty;

public class TipOpenHelper {

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TipAbs.TABLE_NAME + " (" +
                    TipAbs._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    TipAbs.TIP_FID + TEXT_TYPE + COMMA_SEP +
                    TipAbs.TIP_NAME + TEXT_TYPE + COMMA_SEP +
                    TipAbs.TIP_DATE + " DATE" + COMMA_SEP +
                    TipAbs.TIP_TIP + " FLOAT" + COMMA_SEP +
                    TipAbs.TIP_PERCENT + " FLOAT" + COMMA_SEP +
                    TipAbs.TIP_TOTAL + " FLOAT" + COMMA_SEP +
                    TipAbs.TIP_LOCATION + TEXT_TYPE + COMMA_SEP +
                    TipAbs.TIP_COORDS + TEXT_TYPE +
                    " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TipAbs.TABLE_NAME;

    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = "OrangePeel.db";
    private static final String TAG = "TipOpenHelper";

    private TipDBHelper mDbHelper;
    private SQLiteDatabase mDb;

    private final Context mCtx;

    private class TipDBHelper extends SQLiteOpenHelper {

        public TipDBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        public TipDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_ENTRIES);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            //This DB should only be a cache for online data, on upgrade should pull from web
            db.execSQL(SQL_DELETE_ENTRIES);
            onCreate(db);
        }

        @Override
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }
    }


    public TipOpenHelper(Context context) {
        this.mCtx = context;
    }

    public TipOpenHelper open() throws SQLException {
        mDbHelper = new TipDBHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    public long createTip(Tip tip) {
        ContentValues initialValues = returnValues(tip);
        return mDb.insert(TipAbs.TABLE_NAME, null, initialValues);
    }

    public boolean updateTip(long rowId, Tip tip) {
        ContentValues args = returnValues(tip);
        return mDb.update(TipAbs.TABLE_NAME, args, TipAbs._ID + "=" + rowId, null) > 0;
    }

    public boolean deleteTip(long rowId) {
        Log.d(TAG, "DelRow=" + rowId);
        return mDb.delete(TipAbs.TABLE_NAME, TipAbs._ID + "=" + rowId, null) > 0;
    }

    public Cursor fetchAllTips() {
        return mDb.query(TipAbs.TABLE_NAME, new String[]{TipAbs._ID, TipAbs.TIP_NAME, TipAbs.TIP_LOCATION, TipAbs.TIP_PERCENT, TipAbs.TIP_TIP, TipAbs.TIP_TOTAL, TipAbs.TIP_DATE}, null, null, null, null, TipAbs._ID + " DESC");
    }

    public void purgeDB() {
        mDb.close();
        mCtx.deleteDatabase(DATABASE_NAME);
        try {
            this.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Fetches the Tip.obj and converts it MySQL compatible
     *
     * @param tip
     * @return ContentValues of tip
     */
    private ContentValues returnValues(Tip tip) {
        Log.d("TipOpenHelper", ">>>>>>>>>>>>>>>>>>>" + tip.toString());
        ContentValues values = new ContentValues();
        if (!empty(tip.getDate())) {
            values.put(TipAbs.TIP_DATE, tip.getDate().toString());
        }
        if (!empty(tip.getfId())) {
            values.put(TipAbs.TIP_FID, tip.getfId());
        }
        if (tip.getTip() > 0) {
            values.put(TipAbs.TIP_TIP, tip.getTip());
        }
        if (tip.getTotal() > 0) {
            values.put(TipAbs.TIP_TOTAL, tip.getTotal());
        }
        if (tip.getPercent() > 0) {
            values.put(TipAbs.TIP_PERCENT, tip.getPercent());
        }
        if (!empty(tip.getLocation())) {
            values.put(TipAbs.TIP_LOCATION, tip.getLocation());
        }
        if (!empty(tip.getGeoCoords())) {
            values.put(TipAbs.TIP_COORDS, tip.getGeoCoords());
        }
        if (!empty(tip.getName())) {
            values.put(TipAbs.TIP_NAME, tip.getName());
        }
        return values;
    }

}
