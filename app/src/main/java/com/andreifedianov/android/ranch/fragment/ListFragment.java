package com.andreifedianov.android.ranch.fragment;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andreifedianov.android.ranch.R;
import com.andreifedianov.android.ranch.database.TipAbs;
import com.andreifedianov.android.ranch.database.TipOpenHelper;
import com.andreifedianov.android.ranch.main.MainActivity;
import com.andreifedianov.android.ranch.models.Tip;
import com.andreifedianov.android.ranch.objects.CustomTextView;

import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ListFragment extends Fragment implements AbsListView.OnItemClickListener, AdapterView.OnItemLongClickListener, View.OnClickListener {

    private static final String TAG = "ListFragment";
    private static final String ARG_PARAM1 = "VenueID";
    private OnFragmentInteractionListener mListener;
    private TipOpenHelper mDbHelper;

    private Tip tip = null;
    //    private ImageButton submitButton;
    private Button delButton;

    private CustomTextView totalTip;
    private CustomTextView totalSpent;
    private CustomTextView avgTip;

    private LinearLayout click4Debug;
    private LinearLayout debugMenu;

    private int debug = 4;

    private Date d = null;
    private SimpleDateFormat sdf;
    private SimpleDateFormat sdf2;

    private NumberFormat mPercentageFormat = NumberFormat.getPercentInstance();

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private SimpleCursorAdapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ListFragment() {
        sdf2 = new SimpleDateFormat("MM/d/yy", Locale.US);
        sdf = new SimpleDateFormat("E MMM d k:m:s ZZZ yyyy");
    }

    public static ListFragment newInstance(String pos) {
        ListFragment fragment = new ListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, pos);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        mPercentageFormat.setMinimumFractionDigits(2);

        if (getArguments() != null) {
            Log.i(TAG, "onCreate=" + getArguments().getString(ARG_PARAM1));
//            Log.i(TAG, ((MainActivity) getActivity()).getmVenueList().get(Integer.parseInt(getArguments().getString(ARG_PARAM1))).getName());
        }

        tip = ((MainActivity) getActivity()).getTip();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_tip_list, container, false);
        Log.d(TAG, "onCreateView");

        totalTip = (CustomTextView) view.findViewById(R.id.textTipTotal);
        totalSpent = (CustomTextView) view.findViewById(R.id.textTotalSpent);
        avgTip = (CustomTextView) view.findViewById(R.id.textTipAvg);
        click4Debug = (LinearLayout) view.findViewById(R.id.click4Debug);
        click4Debug.setOnClickListener(this);
        debugMenu = (LinearLayout) view.findViewById(R.id.debug_layout);
        delButton = (Button) view.findViewById(R.id.btnDelete);
        delButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDbHelper.purgeDB();
                refreshList(view);
            }
        });
        refreshList(view);
        return view;
    }

    private void refreshList(View view) {
        if (view != null)
            mListView = (AbsListView) view.findViewById(R.id.tip_list);
        if (mListView != null) {
            mAdapter = fillList();
            mListView.setAdapter(mAdapter);
        } else {
            Log.e(TAG, "No List to Set");
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater menuInflater = getActivity().getMenuInflater();
        menuInflater.inflate(R.menu.tip_list_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Log.d(TAG, "onContextItemSelected=" + item.getItemId());
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.remove_item:
                boolean done = mDbHelper.deleteTip(info.id);
                Log.d(TAG, "Deleting=" + info.id + ", done=" + done);
                mAdapter.notifyDataSetChanged();
                refreshList(null);
                return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onStart() {
        getActivity().setTitle(getResources().getString(R.string.title_fragment_tip));
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        registerForContextMenu(mListView);
    }

    @Override
    public void onAttach(Activity activity) {
        Log.d(TAG, "onAttach");
        super.onAttach(activity);
        mDbHelper = new TipOpenHelper(activity.getApplicationContext());
        try {
            mDbHelper.open();
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private SimpleCursorAdapter fillList() {
        if (mDbHelper == null) {
            mDbHelper = new TipOpenHelper(getActivity().getApplicationContext());
        }
        SimpleCursorAdapter adapter = null;
        Cursor c = null;
        try {
            c = mDbHelper.fetchAllTips();
            /**
             * Deserves a new thread
             */
//        Double.valueOf(new DecimalFormat("#.##").format(bill * percent))
            double tipTotal = 0.0;
            double amountTotal = 0.0;
            double tipAvg = 0.0;
            c.moveToFirst();
            while (!c.isAfterLast()) {
                tipTotal += c.getDouble(c.getColumnIndex(TipAbs.TIP_TIP));
                amountTotal += c.getDouble(c.getColumnIndex(TipAbs.TIP_TOTAL));
                tipAvg += c.getDouble(c.getColumnIndex(TipAbs.TIP_PERCENT));
                c.moveToNext();
            }
            c.moveToFirst();
            tipAvg = tipAvg / c.getCount();

            avgTip.setText("" + Double.valueOf(new DecimalFormat("#.##").format(tipAvg * 100)) + "%");
            totalSpent.setText("$" + Double.valueOf(new DecimalFormat("#.##").format(amountTotal + tipTotal)));
            totalTip.setText("$" + Double.valueOf(new DecimalFormat("#.##").format(tipTotal)));

            if (c != null) {
                String[] from = new String[]{
                        TipAbs.TIP_NAME,
                        TipAbs.TIP_DATE,
                        TipAbs.TIP_TIP,
                        TipAbs.TIP_TOTAL,
                        TipAbs.TIP_LOCATION,
                        TipAbs.TIP_PERCENT
                };
                int[] to = new int[]{
                        R.id.tip_name,
                        R.id.tip_date,
                        R.id.tip_amount,
                        R.id.tip_total,
                        R.id.tip_location,
                        R.id.tip_percent
                };
                adapter = new SimpleCursorAdapter(getActivity().getApplicationContext(), R.layout.fragment_tip_list_entry, c, from, to, 0) {
                    @Override
                    public void setViewText(TextView v, String text) {
                        super.setViewText(v, convText(v, text));
                    }

                    private String convText(TextView v, String text) {
                        try {
                            switch (v.getId()) {
                                case R.id.tip_total:
                                    return NumberFormat.getCurrencyInstance().format(Double.valueOf(text));
                                case R.id.tip_amount:
                                    return NumberFormat.getCurrencyInstance().format(Double.valueOf(text));
                                case R.id.tip_percent:
                                    return mPercentageFormat.format(Double.valueOf(text));
                                case R.id.tip_date:
                                    //Tue Feb 10 21:09:44 EST 2015
                                    d = sdf.parse(text);
                                    return sdf2.format(d);
                            }
                            return text;
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                };
            } else {
                Log.e("Cursor", "is null");
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return adapter;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "onClick=" + position);
        if (null != mListener) {

            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onFragmentInteraction("" + position);
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "onLongClick=" + position);
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.click4Debug:
                debug--;
                if (debug <= 0) {
                    debugMenu.setVisibility(LinearLayout.VISIBLE);
                } else {
                    if (debug < 3) {
                        Toast.makeText(getActivity().getApplicationContext(), "Click " + debug + " more times to access debug menu", Toast.LENGTH_SHORT).show();
                    }
                }
        }

    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String id);
    }
}
