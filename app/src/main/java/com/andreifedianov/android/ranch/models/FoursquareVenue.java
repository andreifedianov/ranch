package com.andreifedianov.android.ranch.models;

import android.os.Parcel;
import android.os.Parcelable;

public class FoursquareVenue implements Parcelable {


    private String name;        //
    private String address;     //
    private String city;        //
    private String category;    //Bar/Restaurant/etc
    private String id;          //FoursquareID
    private String dist;           // Approx Distance in Meters

    public FoursquareVenue() {
        this.setId("");
        this.setName("");
        this.setAddress("");
        this.setCity("");
        this.setCategory("");

    }

    public FoursquareVenue(Parcel in) {
        this.readFromParcel(in);


    }

    public String getCity() {
        if (city.length() > 0) {
            return city;
        }
        return city;
    }

    public void setCity(String city) {
        if (city != null) {
            this.city = city.replaceAll("\\(", "").replaceAll("\\)", "");
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDist() {
        return dist;
    }

    public void setDist(String dist) {
        this.dist = dist;
    }

    @Override
    public String toString() {
        return "FoursquareVenue{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", category='" + category + '\'' +
                ", id='" + id + '\'' +
                ", dist='" + dist + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 6;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.getId());
        dest.writeString(this.getName());
        dest.writeString(this.getAddress());
        dest.writeString(this.getCity());
        dest.writeString(this.getCategory());
        dest.writeString(this.getDist());
    }

    private void readFromParcel(Parcel in) {
        this.setId(in.readString());
        this.setName(in.readString());
        this.setAddress(in.readString());
        this.setCity(in.readString());
        this.setCategory(in.readString());
        this.setDist(in.readString());
    }


    public static final Creator CREATOR = new Creator() {
        @Override
        public Object createFromParcel(Parcel source) {
            return new FoursquareVenue(source);
        }

        @Override
        public FoursquareVenue[] newArray(int size) {
            return new FoursquareVenue[size];
        }
    };

}
