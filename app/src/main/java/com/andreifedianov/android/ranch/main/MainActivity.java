package com.andreifedianov.android.ranch.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.andreifedianov.android.ranch.R;
import com.andreifedianov.android.ranch.fragment.ListFragment;
import com.andreifedianov.android.ranch.fragment.LocationFragment;
import com.andreifedianov.android.ranch.fragment.SelectorFragment;
import com.andreifedianov.android.ranch.models.FoursquareVenue;
import com.andreifedianov.android.ranch.models.Tip;
import com.andreifedianov.android.ranch.objects.FadeImageView;
import com.andreifedianov.android.ranch.services.FoursquareService;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import static com.andreifedianov.android.util.Util.empty;

public class MainActivity extends ActionBarActivity implements SelectorFragment.OnTrackingFragmentInteractionListener, LocationFragment.OnLocationFragmentInteractionListener, ListFragment.OnFragmentInteractionListener {
    private static final String TAG = "MainActivity";
    private BroadcastReceiver imageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive");
            if (intent.getAction().equalsIgnoreCase(FoursquareService.FOURSQUARE_IMAGE)) {
                final String url = intent.getStringExtra(FoursquareService.EXTRA_FOURSQUARE_IMAGE_URL);
                Log.d(TAG, "URL=" + url);
                try {
                    if (!empty(url)) {
                        Log.d(TAG, "not null");
                        new DownloadImageTask().execute(url);
                    } else {
                        Log.d(TAG, "null");
                        mImageView.setImageBitmap(null);
                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }
        }
    };
    public Tip tip = new Tip();
    private ArrayList<FoursquareVenue> mVenueList = new ArrayList<>();
    private ArrayList<String> mTitleList = new ArrayList<>();
    private FadeImageView mImageView = null;
    private RenderScript rs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);
        mImageView = (FadeImageView) findViewById(R.id.imageView);
        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            ft.add(R.id.container, LocationFragment.newInstance());
            ft.commit();
        }
        rs = RenderScript.create(getApplicationContext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent = null;
        switch (id) {
            case R.id.menu_item_about:
                intent = new Intent(this, AboutActivity.class);
                break;
            case R.id.menu_item_settings:
                intent = new Intent(this, SettingsActivity.class);
                return false; //disables this until it's ready
        }
        startActivity(intent);
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart");
        IntentFilter backgroundImageFilter = new IntentFilter();
        backgroundImageFilter.addAction(FoursquareService.FOURSQUARE_IMAGE);
        registerReceiver(imageReceiver, backgroundImageFilter);
        super.onStart();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop");
        unregisterReceiver(imageReceiver);
        super.onStop();
    }

    @Override
    public void onBackPressed() {
//        if (mViewPager.getCurrentItem() == 0) {
        super.onBackPressed();
//        } else {
//            mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
//        }
    }

    @Override
    public void onLocationFragmentInteraction(Uri uri) { //Tracking Fragments
        Log.d(TAG, uri.toString());

        String params = uri.getQueryParameter("key");
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        if (params.equals("skip")) {
            ft.replace(R.id.container, ListFragment.newInstance("-1"));

        } else {
            ft.replace(R.id.container, SelectorFragment.newInstance(params));
        }
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onFragmentInteraction(String id) {
        Log.d("TipFragment", "id=" + id);
    }

    @Override
    public void onTrackingFragmentInteraction(Uri uri) {
        Log.d(TAG, "" + uri.getQueryParameter("key"));
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        ft.replace(R.id.container, ListFragment.newInstance(uri.getQueryParameter("key")));
        getSupportFragmentManager().popBackStack();
        ft.addToBackStack(null);
        ft.commit();
    }

    public Tip getTip() {
        if (tip == null) {
            tip = new Tip();
        }
        return tip;
    }

    public void setTip(Tip tip) {
        Log.d("Object", tip.toString());
        this.tip = tip;
    }

    public ArrayList<String> getmTitleList() {
        if (mTitleList == null)
            mTitleList = new ArrayList<>();
        return mTitleList;
    }

    public void setmTitleList(ArrayList<String> mTitleList) {
        this.mTitleList = mTitleList;
    }

    public ArrayList<FoursquareVenue> getmVenueList() {
        if (mVenueList == null)
            mVenueList = new ArrayList<>();
        return mVenueList;
    }

    public void setmVenueList(ArrayList<FoursquareVenue> mVenueList) {
        this.mVenueList = mVenueList;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        public DownloadImageTask() {
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            InputStream in;
            try {
                long first = System.currentTimeMillis();
                in = new URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);

                //this will blur the bitmapOriginal with a radius of 8 and save it in bitmapOriginal
                final Allocation input = Allocation.createFromBitmap(rs, mIcon11); //use this constructor for best performance, because it uses USAGE_SHARED mode which reuses memory

                final Allocation output = Allocation.createTyped(rs, input.getType());
                final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
                script.setRadius(16f);
                script.setInput(input);
                script.forEach(output);
                output.copyTo(mIcon11);
                long last = System.currentTimeMillis();

                long total = last - first;
                Log.d(TAG, "Time to render=" + total);
            } catch (Exception e) {

                System.out.println(e.toString());
            }
            return mIcon11;
        }


/*    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        showProgressDialog();
    }*/

        protected void onPostExecute(Bitmap result) {
            //pDlg.dismiss();
            mImageView.setImageBitmap(result);
        }
    }
}
