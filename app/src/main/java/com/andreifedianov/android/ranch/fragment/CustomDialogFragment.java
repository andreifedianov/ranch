package com.andreifedianov.android.ranch.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.andreifedianov.android.ranch.R;
import com.andreifedianov.android.ranch.models.Constants;
import com.andreifedianov.android.ranch.objects.CustomTextView;

import java.text.NumberFormat;

public class CustomDialogFragment extends DialogFragment implements Dialog.OnClickListener, View.OnClickListener {

    private static final String TAG = "CustomDialogFragment";

    String cleanString = "0";
    OnNumpadDialogListener mListener;

    CustomTextView mInput;
    Button button0;
    Button button1;
    Button button2;
    Button button3;
    Button button4;
    Button button5;
    Button button6;
    Button button7;
    Button button8;
    Button button9;
    Button button00;
    ImageButton buttonDelete;
    ImageButton buttonClear;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_numpad, null);

        button0 = (Button) view.findViewById(R.id.button0);
        button1 = (Button) view.findViewById(R.id.button1);
        button2 = (Button) view.findViewById(R.id.button2);
        button3 = (Button) view.findViewById(R.id.button3);
        button4 = (Button) view.findViewById(R.id.button4);
        button5 = (Button) view.findViewById(R.id.button5);
        button6 = (Button) view.findViewById(R.id.button6);
        button7 = (Button) view.findViewById(R.id.button7);
        button8 = (Button) view.findViewById(R.id.button8);
        button9 = (Button) view.findViewById(R.id.button9);
        button00 = (Button) view.findViewById(R.id.button00);
        buttonDelete = (ImageButton) view.findViewById(R.id.buttonDelete);
        buttonClear = (ImageButton) view.findViewById(R.id.buttonClear);

        button0.setOnClickListener(this);
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        button7.setOnClickListener(this);
        button8.setOnClickListener(this);
        button9.setOnClickListener(this);
        button00.setOnClickListener(this);
        buttonDelete.setOnClickListener(this);
        buttonClear.setOnClickListener(this);

        mInput = (CustomTextView) view.findViewById(R.id.input);

        builder.setView(view)
                .setPositiveButton("Done", this)
                .setNegativeButton("Cancel", this);
        if (getArguments() != null) {
            cleanString = getArguments().getString(Constants.NumpadDialogFragment.NUMPAD_DATA);
            Log.d(TAG, "NoncleanString=" + cleanString);
            setNewInput((cleanString).replaceAll("[$,.]", ""));
        }
        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = ((OnNumpadDialogListener) getTargetFragment());
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Log.d(TAG, dialog.toString() + " " + which);
        if (which == DialogInterface.BUTTON_POSITIVE) {
            mListener.onPositiveDialogClick(CustomDialogFragment.this);
        }
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "Id=" + v.getId());
        int id = v.getId();
        switch (id) {
            case R.id.button0:
                updateTip("0");
                break;
            case R.id.button1:
                updateTip("1");
                break;
            case R.id.button2:
                updateTip("2");
                break;
            case R.id.button3:
                updateTip("3");
                break;
            case R.id.button4:
                updateTip("4");
                break;
            case R.id.button5:
                updateTip("5");
                break;
            case R.id.button6:
                updateTip("6");
                break;
            case R.id.button7:
                updateTip("7");
                break;
            case R.id.button8:
                updateTip("8");
                break;
            case R.id.button9:
                updateTip("9");
                break;
            case R.id.button00:
                updateTip("00");
                break;
            case R.id.buttonDelete:
                deleteInteger(false);
                break;
            case R.id.buttonClear:
                deleteInteger(true);
                break;
        }

        Log.d(TAG, "onNumpadFragmentInteraction=" + id);
    }

    private void updateTip(String num) {
        try {
            if (!cleanString.equalsIgnoreCase(num)) {
                cleanString = (cleanString).replaceAll("[$,.]", "") + num;
                setNewInput(cleanString);
            } else {
                Log.d(TAG, "nope");
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            mInput.setText(null);
        }
    }

    private void deleteInteger(boolean clear) {
        try {
            if (cleanString.length() > 0 && !clear) {
                cleanString = (cleanString).replaceAll("[$,.]", "");
                cleanString = cleanString.substring(0, cleanString.length() - 1);
                if (cleanString.length() <= 0) {
                    cleanString = "0";
                }
            } else {
                cleanString = "0";
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            cleanString = "0";
        } finally {
            setNewInput(cleanString);
        }
    }

    private void setNewInput(String cleanString) {
        double parsed = Double.parseDouble(cleanString);
        String formatted = NumberFormat.getCurrencyInstance().format((parsed / 100));
        mInput.setText(formatted);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        Log.d(TAG, "onCancel");
        return;
//        super.onCancel(dialog);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        Log.d(TAG, "onDismiss");
        super.onDismiss(dialog);
    }

    public String getAmount() {
        return cleanString;
    }

    public interface OnNumpadDialogListener {
        public void onPositiveDialogClick(CustomDialogFragment dialog);
    }
}
