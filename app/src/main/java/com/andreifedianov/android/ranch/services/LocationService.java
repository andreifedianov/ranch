package com.andreifedianov.android.ranch.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class LocationService extends Service implements LocationListener {

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0;  // in meters
    private static final long MIN_TIME_BW_UPDATES = 1000;           // in milliseconds
    private static final float MIN_ALLOWED_ACCURACY = 200;          // in meters
    private static final int TWO_MINUTES = 1000 * 60 * 2;
    private static final String TAG = "LocationService";
    public static String TRANSACTION_DONE = "com.andreifedianov.android.celery.services.action.LOCATION_DONE";
    public static String TRANSACTION_LOCATION_SERVICES_OFF = "com.andreifedianov.android.celery.services.action.LOCATION_SERVICES_OFF";
    private static int NUM_UPDATES = 3;

    protected LocationManager locationManager;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean canGetLocation = false;

    private Location lastKnownLocation = null;
    private double latitude;
    private double longitude;

    private Intent done = null;

    public LocationService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("LocationService", "Service Started");
        done = new Intent(TRANSACTION_DONE);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.getLocation();
        return super.onStartCommand(intent, flags, startId);
    }

    public Location getLocation() {

        try {
            if (locationManager == null) {
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            }
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                done = new Intent(TRANSACTION_LOCATION_SERVICES_OFF); // might not work may have to send an intent back
                sendBroadcast(done);
                stopSelf();
            } else {
                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    Log.d(TAG, "Network Location");
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (lastKnownLocation != null) {
                        longitude = lastKnownLocation.getLongitude();
                        latitude = lastKnownLocation.getLatitude();
                    }
                }
                if (isGPSEnabled) {
                    Log.d(TAG, "GPS Location");
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (lastKnownLocation != null) {
                        longitude = lastKnownLocation.getLongitude();
                        latitude = lastKnownLocation.getLatitude();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lastKnownLocation;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("LocationService", "Service Finished");
        locationManager.removeUpdates(this);

    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }


    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged=" + location.toString() + ", Counter=" + NUM_UPDATES);
        NUM_UPDATES--;
        if (location.getAccuracy() < MIN_ALLOWED_ACCURACY || NUM_UPDATES <= 0)
            if (isBetterLocation(location, lastKnownLocation)) {
                Log.d(TAG, "isBetter=" + location.toString());
                done.putExtra("lat", location.getLatitude());
                done.putExtra("lon", location.getLongitude());
                done.putExtra("Provider", location.getProvider());
                sendBroadcast(done);
                stopSelf();
            } else {
                Log.d(TAG, "isWorse=" + location.toString());
            }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d(TAG, "onStatusChanged=" + provider + ", status=" + status);
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d(TAG, "onProviderEnabled=" + provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d(TAG, "onProviderDisabled=" + provider);
    }


    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

}