package com.andreifedianov.android.ranch.models;


public final class Constants {

    private Constants() {
        throw new AssertionError();
    }

    //https://api.foursquare.com/v2/venues/search?client_id=KFFRYVMLJMY4B0H3PDSUUX0QIZBPNLXDKXHQMNZ2F2NHLHBP&client_secret=O4Q4UUP1TMASWJEKCB4XF5V5EWYKQCR4WW4ICUM4AFDYV5P2&v=20150120&ll=33.89340957,-84.28405526&venue=checkin
    // 22.28207497,114.15761565
    //33.89340957 -84.28405526
    //33.78779543,-84.39789937
    //33.893313,-84.284223

    public static final String CLIENT_ID = "KFFRYVMLJMY4B0H3PDSUUX0QIZBPNLXDKXHQMNZ2F2NHLHBP";
    public static final String CLIENT_SECRET = "O4Q4UUP1TMASWJEKCB4XF5V5EWYKQCR4WW4ICUM4AFDYV5P2";
    public static final String FOURSQUARE_URL = "https://api.foursquare.com/v2/venues/search";
    public static final String FOURSQUARE_IMAGE_URL = "https://api.foursquare.com/v2/venues/";
    public static final String PHOTOS = "/photos";
    public static final String ll = "0.0,0.0"; //Lat,Lon
    public static final Double llAcc = 100.0; //Accuracy
    public static final String query = "donuts"; //Type of places to look for
    public static final int limit = 10; //# of results
    public static final String intent = "checkin"; //https://developer.foursquare.com/docs/venues/search
    public static final int radius = 800; //radius of results
    public static final String version = "20150202"; //YYYYMMDD
    public static final String category_food = "4d4b7105d754a06374d81259"; //Food Category


    public class NumpadDialogFragment {
        public static final String NUMPAD_DATA = "NUMPAD_DATA";
    }
}
