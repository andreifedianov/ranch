package com.andreifedianov.android.ranch.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

import com.andreifedianov.android.ranch.R;
import com.andreifedianov.android.ranch.objects.CustomTextView;
import com.andreifedianov.android.ranch.objects.Dynamics;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class TipSlider extends FrameLayout {


    public static final int TOUCH_ROUND = 2;
    private static final int TOUCH_ABOVE = 1;
    private static final int TOUCH_CENTER = 0;
    private static final int TOUCH_BELOW = -1;
    private static final String TAG = "TipSlider";
    private static final double UPPER = 33;
    private static final double HALF = 0.5;
    private double percent = 15;
    private double amount = 0;
    private double total = -1;

    private Dynamics childTop = new Dynamics(400, 0.7f);

    private View child; //--@+id/inputTipSelect
    private CustomTextView percentView;
    private CustomTextView amountView;
    private OnSliderUpdate mListener;


    private int touchStartY;

    private int childTopStart;

    private boolean touched;

    private VelocityTracker velocityTracker;

    private Runnable expandAnimator = new Runnable() {
        @Override
        public void run() {
            childTop.update(AnimationUtils.currentAnimationTimeMillis());
            if (!childTop.isAtRest()) {
                postDelayed(this, 16);
            }
            requestLayout();
        }
    };

    public TipSlider(Context context) {
        super(context);
    }

    public TipSlider(Context context, AttributeSet attrs) {
        super(context, attrs);
        setWillNotDraw(false);
    }

    public TipSlider(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setWillNotDraw(false);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TipSlider(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setWillNotDraw(false);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        Log.d(TAG, "Event=" + event.toString());
        int where = isOnSelector(event);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                switch (where) {
                    case TOUCH_ABOVE:
                        Log.d(TAG, "Above");
                        updateTip(TOUCH_ABOVE);
                        return false;
                    case TOUCH_CENTER:
                        Log.d(TAG, "CENTER");
                        startTouch(event);
                        return true;
                    case TOUCH_BELOW:
                        updateTip(TOUCH_BELOW);
                        Log.d(TAG, "Below");
                        return false;
                }
            case MotionEvent.ACTION_MOVE:
                handleMove(event);
                return true;

            default:
                endTouch(event);
                return true;

        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Log.d(TAG, "onSizeChanged");
//        mTipLayout.setY((float) (percent / UPPER_LIMIT * mTipContainer.getHeight() * 100f) - (mTipLayout.getHeight() / 2));
        float position = ((float) (percent / UPPER * this.getHeight()) - (child.getHeight() / 2));
        childTop.setPosition(position, AnimationUtils.currentAnimationTimeMillis());
//        childTop.setPosition(open ? 0 : -child.getMeasuredHeight(), AnimationUtils.currentAnimationTimeMillis());
        Log.v(TAG, "w=" + child.getWidth() + ", h=" + child.getHeight());
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        Log.d(TAG, "onFinishInflate");
        child = getChildAt(0);
        percentView = (CustomTextView) findViewById(R.id.inputTipPercent);
        amountView = (CustomTextView) findViewById(R.id.inputTipAmount);
        Log.v(TAG, "w=" + child.getWidth() + ", h=" + child.getHeight());
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.makeMeasureSpec(getMeasuredWidth(), MeasureSpec.EXACTLY);
        int height = MeasureSpec.makeMeasureSpec(getMeasuredHeight(), MeasureSpec.AT_MOST);
        getChildAt(0).measure(width, height);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        int cTop = (int) childTop.getPosition();
        child.layout(0, cTop, child.getWidth(), cTop + child.getHeight());
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    private int isOnSelector(MotionEvent event) { // return 1 if above, 0 if on the selector, or -1 if below
//        Log.d(TAG, "isOnSelector Event=" + event.toString());
        if (event.getY() > (child.getY() + child.getHeight())) {
            return TOUCH_BELOW;
        } else if (event.getY() < (child.getY())) {
            return TOUCH_ABOVE;
        } else {
            return TOUCH_CENTER;
        }
    }

    private void startTouch(MotionEvent event) {
        Log.d(TAG, "startTouch" + childTop.toString());
        velocityTracker = VelocityTracker.obtain();
        velocityTracker.addMovement(event);
        removeCallbacks(expandAnimator);
        touchStartY = (int) event.getY();
        childTopStart = (int) childTop.getPosition();
        touched = true;
        invalidate();
    }

    private void handleMove(MotionEvent event) {
//        Log.d(TAG, "handleMove" + childTop.toString());
        velocityTracker.addMovement(event);
        int diff = (int) (event.getY() - touchStartY);
        long now = AnimationUtils.currentAnimationTimeMillis();

        int targetPos = childTopStart + diff;
        if (targetPos > (this.getHeight() - child.getHeight()) || targetPos < 0) {
            if (targetPos > this.getHeight() - child.getHeight()) {
                targetPos = this.getHeight() - child.getHeight();
            } else if (targetPos < 0) {
                targetPos = 0;
            }
        }
        childTop.setPosition(targetPos, now);
        updateTip();
    }

    private void endTouch(MotionEvent event) {
        Log.d(TAG, "endTouch" + childTop.toString());
        touched = false;
        velocityTracker.addMovement(event);
        velocityTracker.computeCurrentVelocity(1000);
        float yVelocity = velocityTracker.getYVelocity();
        long now = AnimationUtils.currentAnimationTimeMillis();
        childTop.setTargetPosition(getTargetPos(), now);
        childTop.setVelocity(yVelocity, now);

        removeCallbacks(expandAnimator);
        post(expandAnimator);

        velocityTracker.recycle();
        velocityTracker = null;
        updateTip();
    }

    private int getTargetPos() {
        int targetPos = Math.round(childTop.getPosition());
        if (targetPos > (this.getHeight() - child.getHeight()) || targetPos < 0) {
            if (targetPos > this.getHeight() - child.getHeight()) {
                targetPos = this.getHeight() - child.getHeight();
            } else if (targetPos < 0) {
                targetPos = 0;
            }
        }
        return targetPos;
    }

    public void updateTip() {
        this.updateTip(0);
    }


    public void updateTip(int touchDirection) {
        float pos;
        long now;
        switch (touchDirection) {
            case TOUCH_ABOVE:
                percent = percent - HALF;
                pos = (float) (percent * this.getHeight() / UPPER);
                now = AnimationUtils.currentAnimationTimeMillis();
                childTop.setPosition(pos, now);
                break;
            case TOUCH_BELOW:
                percent = percent + HALF;
                pos = (float) (percent * this.getHeight() / UPPER);
                now = AnimationUtils.currentAnimationTimeMillis();
                childTop.setPosition(pos, now);
                break;
            case TOUCH_ROUND:
                Log.d(TAG, "total=" + total + ", bill=" + amount);
                if (total >= 0 && total != Math.round(total)) {
                    total = Math.round(total);
                    percent = ((total / amount) - 1) * 100;
                    pos = (float) (percent * this.getHeight() / UPPER);
                    now = AnimationUtils.currentAnimationTimeMillis();
                    childTop.setPosition(pos, now);
                }
                break;
            default:
                float position = (!touched ? childTop.getTargetPos() : childTop.getPosition());
                percent = Math.round(position * UPPER / this.getHeight());
                break;
        }
        updateAmount();
        requestLayout();
    }


    private void updateAmount() {
        percentView.setText("" + Double.valueOf(new DecimalFormat("#.##").format(percent)) + "%");
        double bill = getAmount();
        double amount = (percent / 100) * bill;
        total = (percent / 100 + 1) * bill;
        String formatted = NumberFormat.getCurrencyInstance().format(amount);
        amountView.setText(formatted);
        mListener.setTotal(total);
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
        updateAmount();
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public void setOnSliderUpdate(OnSliderUpdate mListener) {
        this.mListener = mListener;
    }

    public interface OnSliderUpdate {
        public void setTotal(double total);
    }
}
