package com.andreifedianov.android.ranch.http;

import android.util.Log;

import org.apache.http.ConnectionClosedException;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import static com.andreifedianov.android.util.Util.empty;

public class HttpBin {
    private static HttpBin inst = null;


    private int CONN_TIMEOUT;
    private int SO_TIMEOUT;

    public HttpBin() {
        CONN_TIMEOUT = 15 * 1000;
        SO_TIMEOUT = 15 * 1000;
    }

    public HttpBin(int CONN_TIMEOUT, int SO_TIMEOUT) {
        this.CONN_TIMEOUT = CONN_TIMEOUT;
        this.SO_TIMEOUT = SO_TIMEOUT;
    }

    public static HttpBin getInstance() {
        if (inst == null) {
            inst = new HttpBin();
        }
        return inst;
    }

    public static HttpBin getInstance(int connectionTimeout, int socketTimeout) {
        if (inst == null) {
            inst = new HttpBin(connectionTimeout, socketTimeout);
        }
        return inst;
    }

    /**
     * Default POST with destination url and params.
     *
     * @param url    - Destination URL
     * @param params - List of params
     * @return Destination Response
     */
    public String post(String url, List<NameValuePair> params) {
        return post(url, null, params, CONN_TIMEOUT, SO_TIMEOUT);
    }

    /**
     * POST with destination url, params and timeouts.
     *
     * @param url           - Destination URL
     * @param params        - List of params
     * @param connTimeout   -
     * @param socketTimeout -
     * @return Destination Response
     */
    public String post(String url, List<NameValuePair> params, int connTimeout, int socketTimeout) {
        return post(url, null, params, connTimeout, socketTimeout);
    }

    /**
     * POST with destination url, backupurl, and params
     *
     * @param url       - Destination URL
     * @param backupUrl - Backup URL in case the first one is not reachable
     * @param params    - List of params
     * @return Destination/Backup Response
     */
    public String post(String url, String backupUrl, List<NameValuePair> params) {
        return post(url, backupUrl, params, CONN_TIMEOUT, SO_TIMEOUT);
    }

    /**
     * POST with destination url, backupurl, params and timeouts
     *
     * @param url           - Destination URL
     * @param backupUrl     - Backup URL in case the first one is not reachable
     * @param params        - List of params
     * @param connTimeout   -
     * @param socketTimeout -
     * @return Destination/Backup Response
     */
    public String post(String url, String backupUrl, List<NameValuePair> params, int connTimeout, int socketTimeout) {
        String destinationUrl = null;
        if (!empty(url) /*&& isReachable(url, connTimeout, socketTimeout)*/) {
            destinationUrl = url;
        } else if (!empty(backupUrl)/* && isReachable(url, connTimeout, socketTimeout)*/) {
            destinationUrl = backupUrl;
        } else {
            return "url unreachable";
        }
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, connTimeout);
        HttpConnectionParams.setSoTimeout(httpParams, socketTimeout);
        HttpClient httpClient = new DefaultHttpClient(httpParams);
        try {
            HttpPost httpPost = new HttpPost(destinationUrl);
            Log.d("url= {}", httpPost.getURI().toASCIIString());
            UrlEncodedFormEntity entity = null;
            if (params != null) {
                entity = new UrlEncodedFormEntity(params, "UTF-8");
                httpPost.setEntity(entity);
            }
            // Get the response:
            HttpResponse res = httpClient.execute(httpPost);
            InputStream in = res.getEntity().getContent();
            ByteArrayOutputStream opStream = new ByteArrayOutputStream();

            int readByte;

            while ((readByte = in.read()) != -1) {
                opStream.write(readByte);
            }
            byte[] data = opStream.toByteArray();
            opStream.close();
            return new String(data);
        } catch (UnsupportedEncodingException e) {
            Log.e("Error", e.getMessage());
        } catch (ClientProtocolException e) {
            Log.e("Error", e.getMessage());
        } catch (IOException e) {
            Log.e("Error", e.getMessage());
        } finally {
            httpClient.getConnectionManager().shutdown();
        }
        return null;
    }

    public String post(String url, String input) {
        return post(url, input, CONN_TIMEOUT, SO_TIMEOUT);
    }

    public String post(String url, String input, int connTimeout, int socketTimeout) {
        HttpParams httpParams = new BasicHttpParams();
        String httpStatus = "";
        HttpConnectionParams.setConnectionTimeout(httpParams, connTimeout);
        HttpConnectionParams.setSoTimeout(httpParams, socketTimeout);
        HttpClient httpClient = new DefaultHttpClient(httpParams);
        try {
            HttpPost httpPost = new HttpPost(url);
            Log.i("urlHttp= {}", httpPost.getURI().toASCIIString());
            StringEntity entity = null;
            if (input != null) {
                entity = new StringEntity(input, "UTF-8");
                httpPost.setEntity(entity);
            }

            HttpResponse res = httpClient.execute(httpPost);
            if (res != null && res.getStatusLine() != null) {
                httpStatus = res.getStatusLine().getStatusCode() + "";
            }
            Log.i("httpStatusResponse: {}", httpStatus);

            InputStream in = res.getEntity().getContent();
            ByteArrayOutputStream opStream = new ByteArrayOutputStream();

            int readByte;

            while ((readByte = in.read()) != -1) {
                opStream.write(readByte);
            }
            byte[] data = opStream.toByteArray();
            opStream.close();

            return new String(data);

        } catch (UnsupportedEncodingException e) {
            Log.e("Error", e.getMessage());
        } catch (ClientProtocolException e) {
            Log.e("Error", e.getMessage());
        } catch (IOException e) {
            Log.e("Error", e.getMessage());
        } finally {
            httpClient.getConnectionManager().shutdown();
        }
        return null;
    }

    public String post(String url, String input, java.util.Map<String, String> headerValues) {
        return post(url, input, headerValues, CONN_TIMEOUT, SO_TIMEOUT);
    }

    public String post(String url, String input, java.util.Map<String, String> headerValues, int connTimeout, int socketTimeout) {
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, connTimeout);
        HttpConnectionParams.setSoTimeout(httpParams, socketTimeout);
        HttpClient httpClient = new DefaultHttpClient(httpParams);
        String httpStatus = "";
        try {
            HttpPost httpPost = new HttpPost(url);
            Log.d("urlA******= {}", httpPost.getURI().toASCIIString());
            StringEntity entity = null;
            if (input != null) {
                entity = new StringEntity(input, "UTF-8");
                httpPost.setEntity(entity);
            }
            if (headerValues != null && headerValues.size() > 0) {
                java.util.Iterator it = headerValues.entrySet().iterator();

                while (it.hasNext()) {
                    Map.Entry<String, String> pairs = (Map.Entry<String, String>) it.next();
                    httpPost.setHeader((String) pairs.getKey(), (String) pairs.getValue());
                    it.remove(); // avoids a ConcurrentModificationException
                }

            }
            HttpResponse res = httpClient.execute(httpPost);
            if (res != null && res.getStatusLine() != null) {
                httpStatus = res.getStatusLine().getStatusCode() + "";
            }
            Log.d("httpStatusResponse {}", httpStatus);
            return httpStatus;
            /*
            InputStream in = res.getEntity().getContent();
			ByteArrayOutputStream opStream = new ByteArrayOutputStream();

			int readByte;

			while ((readByte = in.read()) != -1) {
				opStream.write(readByte);
			}
			byte[] data = opStream.toByteArray();
			opStream.close();
			
			return new String(data);*/
        } catch (UnsupportedEncodingException e) {
            Log.i("Error", e.getMessage());
        } catch (ClientProtocolException e) {
            Log.i("Error", e.getMessage());
        } catch (IOException e) {
            Log.i("Error", e.getMessage());
        } finally {
            httpClient.getConnectionManager().shutdown();
        }
        return null;
    }
//
//    public boolean isReachable(String url, int connTimeout) {
//        if (connTimeout < 0) connTimeout = this.CONN_TIMEOUT;
//        boolean reachable = false;
//        try {
//            InetAddress addr = InetAddress.getByName(url);
//            reachable = addr.isReachable(connTimeout);
//        } catch (UnknownHostException e) {
//            reachable = false;
//        } catch (IOException e) {
//            reachable = false;
//        }
//        return reachable;
//    }
//
//    public boolean isReachable(String url, int connTimeout, int socketTimeout) {
//
//        HttpParams httpParams = new BasicHttpParams();
//        HttpConnectionParams.setConnectionTimeout(httpParams, connTimeout);
//        HttpConnectionParams.setSoTimeout(httpParams, socketTimeout);
//        HttpClient httpClient = new DefaultHttpClient(httpParams);
//
//        HttpGet httpGet = new HttpGet(url);
//
//        try {
//            HttpResponse resp = httpClient.execute(httpGet);
//            HttpEntity entity = resp.getEntity();
//            if (entity != null) {
//                return true;
//            }
//        } catch (UnknownHostException e) {
//            Log.e("Error", e.getMessage());
//            return false;
//        } catch (ConnectException e) {
//            Log.e("Error", e.getMessage());
//            return false;
//        } catch (ClientProtocolException e) {
//            Log.e("Error", e.getMessage());
//            return false;
//        } catch (IOException e) {
//            Log.e("Error", e.getMessage());
//            return false;
//        }
//        return false;
//    }
    //	public boolean isReachable(String url, int connTimeout, int socketTimeout, boolean local){
    //		if(local)
    //			return true;
    //		HttpParams httpParams = new BasicHttpParams();
    //		HttpConnectionParams.setConnectionTimeout(httpParams, connTimeout);
    //		HttpConnectionParams.setSoTimeout(httpParams, socketTimeout);
    //		HttpClient httpClient = new DefaultHttpClient(httpParams);
    //
    //		HttpGet httpGet = new HttpGet(url);
    //
    //		try {
    //			HttpResponse resp = httpClient.execute(httpGet);
    //			HttpEntity entity = resp.getEntity();
    //			if(entity != null){
    //				return true;
    //			}
    //		} catch (UnknownHostException e){
    //			Log.e("Error", e.getMessage());
    //			return false;
    //		} catch (ConnectException e){
    //			Log.e("Error", e.getMessage());
    //			return false;
    //		} catch (ClientProtocolException e) {
    //			Log.e("Error", e.getMessage());
    //			return false;
    //		} catch (IOException e) {
    //			Log.e("Error", e.getMessage());
    //			return false;
    //		}
    //		return false;
    //	}


    public String get(String url) {
        return get(url, CONN_TIMEOUT, SO_TIMEOUT);
    }


    public String get(String url, int connTimeout, int socketTimeout) {
        final StringBuilder b = new StringBuilder();
        try {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, connTimeout);
            HttpConnectionParams.setSoTimeout(httpParams, socketTimeout);
            HttpClient httpClient = new DefaultHttpClient(httpParams);

            HttpGet httpGet = new HttpGet(url);
            HttpResponse res = httpClient.execute(httpGet);
            HttpEntity entity = res.getEntity();
            InputStream iS = entity.getContent();


//            final ContentType contentType = ContentType.getOrDefault(entity);
//            Charset charset = contentType.getCharset();
//
//            if (charset == null) {
//                charset = HTTP.DEF_CONTENT_CHARSET;
//            }
            final char[] tmp = new char[1024];
//            final Reader reader = new InputStreamReader(iS, charset);
            final Reader reader = new InputStreamReader(iS);
            int l;
            while ((l = reader.read(tmp)) != -1) {
                b.append(tmp, 0, l);
            }

            //			ByteArrayOutputStream opStream = new ByteArrayOutputStream();
            //
            //			int readByte;
            //
            //			while ((readByte = iS.read()) != -1) {
            //				opStream.write(readByte);
            //			}
            //			data = opStream.toByteArray();
            //			opStream.close();
            //			return new String(data);
        } catch (final ConnectionClosedException ignore) {
            Log.e("Ignore", ignore.getMessage());
        } catch (ClientProtocolException e) {
            Log.e("Error", e.getMessage());
        } catch (IllegalStateException e) {
            Log.e("Error", e.getMessage());
        } catch (IOException e) {
            Log.e("Error", e.getMessage());
        }
        return b.toString();


    }


    public String getHeader(String url, String header) {
        return getHeader(url, header, CONN_TIMEOUT, SO_TIMEOUT);
    }


    public String getHeader(String url, String header, int connTimeout, int socketTimeout) {
        try {
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, connTimeout);
            HttpConnectionParams.setSoTimeout(httpParams, socketTimeout);
            HttpClient httpClient = new DefaultHttpClient(httpParams);

            HttpGet httpGet = new HttpGet(url.toString());
            HttpResponse res = httpClient.execute(httpGet);
            Header[] h = res.getHeaders(header);
            if (h.length > 0) {
                return h[0].getValue();
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
