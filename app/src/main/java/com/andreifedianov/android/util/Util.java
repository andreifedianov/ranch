package com.andreifedianov.android.util;

import android.util.Log;

import java.util.Date;

public final class Util {

    private static String TAG = "Util";

    private Util() {
    }

    static public boolean empty(String str) {
        return str == null ? true : str.trim().length() == 0;
    }

    static public boolean empty(Date date) {
        return date == null ? true : date.toString().length() == 0;
    }


//    static public boolean empty(String input) {
//        try {
//            return input.isEmpty() || input == null;
//        } catch (Exception e) {
//            Log.e(TAG, e.getMessage());
//            return true;
//        }
//    }


    static public boolean isNumeric(String input) {
        try {
            double d = Double.parseDouble(input);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return false;
        }
        return true;
    }
}
