# Ranch - Smart Tip Calculator #
Ranch will revolutionize the way you tip. Pick where you are, enter your bill and slide to select a tip percentage. Ranch tracks your totals, percentages, and how much you tip at your favorite places.

[Play Store](https://play.google.com/store/apps/details?id=com.andreifedianov.android.ranch)

[Ranch: To-Do](http://www.evernote.com/l/ALZFoKmLB-5J1YHAVWbG9dCOraNeZGeOboY/)


[Markdown for Reference](https://bitbucket.org/tutorials/markdowndemo)